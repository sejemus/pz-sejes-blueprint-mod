require "server/Items/SuburbsDistributions";
print("Seje's Blueprint Mod :: Adding stuffs to world generation")

Items = {}

table.insert(SuburbsDistributions["shed"]["counter"].items, "Items.MetalFragments");
table.insert(SuburbsDistributions["shed"]["counter"].items, 10);
table.insert(SuburbsDistributions["shed"]["counter"].items, "Items.MetalSheet");
table.insert(SuburbsDistributions["shed"]["counter"].items, 1);
table.insert(SuburbsDistributions["shed"]["counter"].items, "Items.MetalBar");
table.insert(SuburbsDistributions["shed"]["counter"].items, 3);
table.insert(SuburbsDistributions["shed"]["other"].items, "Items.Stick");
table.insert(SuburbsDistributions["shed"]["other"].items, 15);

table.insert(SuburbsDistributions["shed"]["other"].items, "Items.MetalFragments");
table.insert(SuburbsDistributions["shed"]["other"].items, 10);
table.insert(SuburbsDistributions["shed"]["other"].items, "Items.MetalSheet");
table.insert(SuburbsDistributions["shed"]["other"].items, 1);
table.insert(SuburbsDistributions["shed"]["other"].items, "Items.MetalBar");
table.insert(SuburbsDistributions["shed"]["other"].items, 3);
table.insert(SuburbsDistributions["shed"]["other"].items, "Items.Stick");
table.insert(SuburbsDistributions["shed"]["other"].items, 15);


table.insert(SuburbsDistributions["all"]["crate"].items, "Items.MetalFragments");
table.insert(SuburbsDistributions["all"]["crate"].items, 10);
table.insert(SuburbsDistributions["all"]["crate"].items, "Items.MetalSheet");
table.insert(SuburbsDistributions["all"]["crate"].items, 1);
table.insert(SuburbsDistributions["all"]["crate"].items, "Items.MetalBar");
table.insert(SuburbsDistributions["all"]["crate"].items, 3);

table.insert(SuburbsDistributions["all"]["bin"].items, "Items.MetalFragments");
table.insert(SuburbsDistributions["all"]["bin"].items, 5);
table.insert(SuburbsDistributions["all"]["bin"].items, "Items.Stick");
table.insert(SuburbsDistributions["all"]["bin"].items, 25);

table.insert(SuburbsDistributions["all"]["shelvesmag"].items, "Items.MetalFragments");
table.insert(SuburbsDistributions["all"]["shelvesmag"].items, 5);

table.insert(SuburbsDistributions["all"]["metal_shelves"].items, "Items.MetalFragments");
table.insert(SuburbsDistributions["all"]["metal_shelves"].items, 5);

table.insert(SuburbsDistributions["gunstorestorage"]["all"].items, "Items.MetalFragments");
table.insert(SuburbsDistributions["gunstorestorage"]["all"].items, 5);
print("Seje's Blueprint Mod :: Done!")
