Heya fellow survivors!

I bring to you, a blueprint mod! 


# What is this?! #

This mod simply adds blueprints to the game. Now you can make your own hammers, axes, pistols and more!

But it's not free of cause. Most importantly, you have to find the item you want to create a blueprint for first, then grab a piece of paper and a pen or pencil and start drawing the item on the paper 

You also need to find metal and sticks in order to craft the tools and such.

 

# Recipes #

Recipes are as follows:

 

## Blueprints: ##

### Axe Blueprint: ###

Axe (kept)

Paper (Used)

Pen or Pencil (kept)

 

### Saw Blueprint: ###

Saw (kept)

Paper (Used)

Pen or Pencil (kept)

 

### Hammer Blueprint: ###

Hammer (kept)

Paper (Used)

Pen or Pencil (kept)

 

### Screwdriver Blueprint: ###

Screwdriver (kept)

Paper (Used)

Pen or Pencil (kept)

 

### Pistol Blueprint: ###

Pistol (kept)

Paper (Used)

Pen or Pencil (kept)

 

### 9mm Ammo Blueprint: ###

9mm Bullets (kept)

Paper (Used)

Pen or Pencil (kept)

 

## Items: ##

### Axe: ###

Axe Blueprint (kept)

30 Metal Fragments or 3 Metal Bars or 1 Metal Sheet (used)

Stick (used)


### Saw: ###

Saw Blueprint (kept)

50 Metal Fragments or 5 Metal Bars or 1 Metal Sheet (used)


### Hammer: ###

Hammer Blueprint (kept)

30 Metal Fragments or 3 Metal Bars or 1 Metal Sheet (used)

Stick (used)


### Screwdriver: ###

Screwdriver Blueprint (kept)

20 Metal Fragments or 2 Metal Bars or 1 Metal Sheet (used)


### Pistol: ###

Pistol Blueprint (kept)

200 Metal Fragments or 20 Metal Bars or 2 Metal Sheet (used)


### 9mm Ammo: ###

9mm Ammo Blueprint (kept)

Metal Fragments (used) (10 fragments makes 10 bullets, 100 fragments makes 100 bullets)

 

## So.. Where do I find this metal and sticks? ##

Good question.

Sticks are commonly found in bins and metal are mostly found in crates, sheds and metal shelves.

Metal Fragments are the most common, then Metal Bars and Lastly Metal Sheets being the most rare.

 

## I have a suggestion for a change, balance or other? ##

Alright then, let me hear!

If you feel anything is overpowered, too common, too rare. Let me know.

Got an idea for a item you should be able to craft? Let me know!

Preferbly on the Indie Stone [forum thread](http://theindiestone.com/forums/index.php/topic/8110-sejes-blueprint-mod/?p=106602)